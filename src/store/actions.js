import { UPDATE_LIST_ITEMS } from './actionTypes';

export const updateListItems = data => {
    return {
        type: UPDATE_LIST_ITEMS,
        payload: data
    }
}